# Gradle MultiModule Project

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
         <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li>
        <a href="#step-by-step-process-of-project-creation">Step by Step Process of Project Creation</a>
        <ul>
        <li><a href="#"></a></li>
         <li><a href="#"></a></li>
        </ul>
    </li>
    <li>
        <a href="#conclusion">Conclusion</a>
    </li>
  </ol>
</details>

### About The Project

### Built With

- Springboot(2.7.2)
- Thymeleaf
- Lombok

### Prerequisites

- IntelliJ IDEA(recommended)
- Java11/OpenJdk11
- Gradle

### Step by Step Process of Project Creation

#### Creating Project with Spring Initializer

Project type Gradle and Update the Group and Artifact info

#### Modify build.gradle

In build.gradle file add subprojects {} then move dependencies and test in it

```gradle
subprojects {

    dependencies {
        compileOnly 'org.projectlombok:lombok'
        annotationProcessor 'org.projectlombok:lombok'
    }
    
    tasks.named('test') {
        useJUnitPlatform()
    }
}
```

Next, add the plugin apply at the beginning of subprojects and add them according to the plugin created by default
initializer.

```gradle
subprojects {
    apply plugin: 'org.springframework.boot'
    apply plugin: 'io.spring.dependency-management'
    apply plugin: 'java'
    
    ...
}
```

The final version will look like this

```gradle
buildscript {
    ext {
        springBootVersion = '2.7.2'
    }
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
    }
}

allprojects {
    group = 'com.springboot.app'
    version = '1.0.0-SNAPSHOT'
}

subprojects {
    apply plugin: 'org.springframework.boot'
    apply plugin: 'io.spring.dependency-management'
    apply plugin: 'java'

    sourceCompatibility = '11'

    repositories {
        mavenCentral()
    }

    configurations {
        compileOnly {
            extendsFrom annotationProcessor
        }
    }

    dependencies {
        //implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
        //implementation 'org.springframework.boot:spring-boot-starter-web'
        compileOnly 'org.projectlombok:lombok'
        //developmentOnly 'org.springframework.boot:spring-boot-devtools'
        annotationProcessor 'org.projectlombok:lombok'
        //testImplementation 'org.springframework.boot:spring-boot-starter-test'
    }

    tasks.named('test') {
        useJUnitPlatform()
    }

    dependencyManagement {
        imports {
            mavenBom("org.springframework.boot:spring-boot-dependencies:${springBootVersion}")
        }
    }
}
```

#### Create Module

create two module named core and web as a gradle project.
By default, it will include those module in settings.gradle file

```gradle
rootProject.name = 'gradle-multi-module-project'
include 'core'
include 'web'
```

in core module build.gradle file remove everything except repositories and dependencies. Add the required dependencies

```gradle
apply plugin: 'org.springframework.boot'

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
}
```

In web module build.gradle file do the same as core module.

```gradle
dependencies {
    implementation project(':core')

    implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
    implementation 'org.springframework.boot:spring-boot-starter-web'
}

```

**Note :** in web module needs to add implementation project(':core') because this one is dependent on the core module.
The " : "  colon used to define a string in groovy

Create packages in both of the modules based on the group " group = 'com.springboot.app' " from root build.gradle file
move the **SpringBootApplication** class to the web module and run clean and build from gradle to make sure everything
is working perfectly.

#### Write Module

Create a package named service in core module.
create a demo service interface and implementation for it.
DemoService interface

```java
public interface DemoService {
    String getMessage();
}
```

DemoServiceImpl class

```java

@Service
public class DemoServiceImpl implements DemoService {
    @Override
    public String getMessage() {
        return "Hello from the Core Module";
    }
}
```

Create a Controller Package in Web module. Add a controller directory and create a DemoController class

```java

@Controller
@RequestMapping("demo")
public class DemoController {

    private final DemoService demoService;

    public DemoController(DemoService demoService) {
        this.demoService = demoService;
    }

    @GetMapping
    public String getMessage(Model model) {
        model.addAttribute("message", demoService.getMessage());
        return "demo/index";
    }
}

```

create a directory demo inside the resources->templates and a file index inside the demo directory

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h2 th:text="${message}">Demo Content</h2>
</body>
</html>
```

#### Run and test

### Conclusion




